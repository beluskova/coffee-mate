package ie.cm.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Coffee  {

	public String _id;
	public String uid;
	public String name;
	public String shop;
	public double rating;
	public double price;
	public boolean favourite;

	public String usertoken;
	public String address;
	public String googlephoto;
	public Marker marker = new Marker();
	public double latitude;
	public double longitude;

	public Coffee(String uid, String name, String shop, double rating, double price, boolean fav, String token,double lat, double lng, String path,String address)
	{
		this.uid = uid;
		this.name = name;
		this.shop = shop;
		this.rating = rating;
		this.price = price;
		this.favourite = fav;

		this.usertoken = token;
		this.address = address;
		this.marker.coords.latitude = lat;
		this.marker.coords.longitude = lng;
		this.latitude = lat;
		this.longitude = lng;
		this.googlephoto = path;
	}

	public Coffee() {

	}

	@Override
	public String toString() {
		return "Coffee [name=" + name
				+ ", shop =" + shop + ", rating=" + rating + ", price=" + price
				+ ", fav =" + favourite + " "
				+ usertoken + " " + address + " " + marker.coords.latitude + " " + marker.coords.longitude + "]";
	}

	@Exclude
	public Map<String, Object> toMap() {
		HashMap<String, Object> result = new HashMap<>();
		result.put("uid", uid);
		result.put("name", name);
		result.put("shop", shop);
		result.put("rating", rating);
		result.put("price", price);
		result.put("favourite", favourite);
		result.put("usertoken", usertoken);
		result.put("googlephoto", googlephoto);
		result.put("address", address);
		result.put("latitude", latitude);
		result.put("longitude", longitude);

		return result;
	}
}

