package ie.cm.models;

public class User {

    public String userId;
    public String userName;
    public String userEmail;
    public String userProfilePic;

    public User(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String userId, String userName, String userEmail, String userProfilePic ){
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userProfilePic = userProfilePic;
    }
}
