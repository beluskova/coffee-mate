package ie.cm.api;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import ie.cm.activities.Login;
import ie.cm.main.CoffeeMateApp;
import ie.cm.models.Coffee;
import ie.cm.models.User;

import static ie.cm.api.CoffeeApi.app;
import static ie.cm.main.CoffeeMateApp.TAG;

public class FBDBManager {

    private static final String TAG = "coffeemate";
    public DatabaseReference mFirebaseDatabase;
    public static String mFBUserId;
    public FBDBListener mFBDBListener;


    public void open() {
        //Set up local caching
        FirebaseApp.initializeApp(CoffeeMateApp.getInstance());
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        //Bind to remote Firebase Database
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        Log.v(TAG, "Database Connected :" + mFirebaseDatabase);
    }

    public void attachListener(FBDBListener listener) {
        mFBDBListener = listener;
    }

    //Check to see if the Firebase User exists in the Database
    //if not, create a new User
    public void checkUser(final String userid,final String username,final String email) {
        Log.v(TAG, "checkUser ID == " + userid);
        mFirebaseDatabase.child("users").child(userid).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v(TAG, "checkUser ID Sucess == " + dataSnapshot);
                        if(dataSnapshot.exists()){
                            Log.v(TAG, "User found : ");
                        }
                        else{
                            Log.v(TAG, "User not found, Creating User on Firebase");
                            User newUser = new User(app.mFirebaseUser.getUid(),
                                    app.mFirebaseUser.getDisplayName(),
                                    app.mFirebaseUser.getEmail(), null);
                                app.mFBDBManager.mFirebaseDatabase.child("users")
                                        .child(app.mFirebaseUser.getUid())
                                        .setValue(newUser);
                        }
                        app.mFBDBManager.mFBUserId = app.mFirebaseUser.getUid();
                     //   mFBDBListener.onSuccess(dataSnapshot);
                        Log.v(TAG, "checkUser ID Sucess == " + dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mFBDBListener.onFailure();
                        Log.v(TAG, "checkUser ID Failure == " + databaseError);
                    }
                }
        );
    }

    private void validateFirebaseUser()
    {
        Log.v(TAG,"Calling validateFirebaseUser() from db manager " );
        if(app.mFirebaseUser == null)
            app.mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        app.mFBDBManager.checkUser(app.mFirebaseUser.getUid(),
                app.mFirebaseUser.getDisplayName(),
                app.mFirebaseUser.getEmail());
    }


    public void addCoffee(final Coffee c)
    {
        mFirebaseDatabase.child("users").child(app.mFBDBManager.mFBUserId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);
                        Log.v(TAG, "User " + c.uid+ " is? equal to "  + app.mFBDBManager.mFBUserId);
                        if (user == null) {
                            // User is null, error out
                            Log.v(TAG, "User " + mFBUserId + " is unexpectedly null");
                            //Toast.makeText(this,
                            //		"Error: could not fetch user.",
                            //		Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new coffee
                            writeNewCoffee(c);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.v(TAG, "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

    private void writeNewCoffee(Coffee c) {
        // Create new coffee at /user-coffees/$userid/$coffeeid and at
        // /coffees/$coffeeid simultaneously
        String key = mFirebaseDatabase.child("coffees").push().getKey();
        Map<String, Object> coffeeValues = new HashMap<>();

        Map<String, Object> childUpdates = new HashMap<>();
        Log.v(TAG, "writing new coffee" + c);
        //All our coffees
        app.mFBDBManager.mFirebaseDatabase.child("coffees")
                .child(key)
                .setValue(c);
        //All coffees per user
        app.mFBDBManager.mFirebaseDatabase.child("user-coffees")
                .child(app.mFBDBManager.mFBUserId)
                .child(key)
                .setValue(c);
       // app.mFirebaseDatabase.updateChildren(childUpdates);
    }

    public Query getAllCoffees()
    {
//        app.mFBDBManager.mFBUserId = app.mFirebaseUser.getUid();
        Query query = mFirebaseDatabase.child("user-coffees").child(mFBUserId)
                .orderByChild("rating");

        return query;
    }

    public Query getFavouriteCoffees()
    {
        Query query = mFirebaseDatabase.child("user-coffees")
                .child(mFBUserId)
                .orderByChild("favourite").equalTo(true);

        return query;
    }

    public void getACoffee(final String coffeeKey)
    {
        Log.v(TAG,"Calling a user from db manager get a coffee ");
        Log.v(TAG,"Calling a user from db manager get a coffee " + coffeeKey);
        mFirebaseDatabase.child("user-coffees").child(app.mFBDBManager.mFBUserId ).child(coffeeKey).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v(TAG,"The read Succeeded: " + dataSnapshot.toString());
                        mFBDBListener.onSuccess(dataSnapshot);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The read failed: " + firebaseError.getMessage());
                        mFBDBListener.onFailure();
                    }
                });
    }

    public void updateACoffee(final String coffeeKey,final Coffee updatedCoffee)
    {
        mFirebaseDatabase.child("user-coffees").child(mFBUserId).child(coffeeKey).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v(TAG,"The update Succeeded: " + dataSnapshot.toString());
                        dataSnapshot.getRef().setValue(updatedCoffee);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The update failed: " + firebaseError.getMessage());
                        mFBDBListener.onFailure();
                    }
                });
    }

    public void toggleFavourite(final String coffeeKey,final boolean isFavourite)
    {
        mFirebaseDatabase.child("user-coffees").child(mFBUserId).child(coffeeKey).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        snapshot.getRef().child("favourite").setValue(isFavourite);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The toggle failed: " + firebaseError.getMessage());
                    }
                });
    }

    public void deleteACoffee(final String coffeeKey)
    {
        mFirebaseDatabase.child("user-coffees").child(mFBUserId).child(coffeeKey).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        snapshot.getRef().removeValue();
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.v(TAG,"The delete failed: " + firebaseError.getMessage());
                    }
                });
    }

    public Query nameFilter(String s)
    {
        Query query = mFirebaseDatabase.child("user-coffees").child(mFBUserId)
                .orderByChild("name").startAt(s).endAt(s+"\uf8ff");

        return query;
    }

    public void getAllCoffeesSnapshot()
    {
        ValueEventListener vel = mFirebaseDatabase.child("user-coffees")
                                            .child(mFBUserId)
                                            .addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mFBDBListener.onSuccess(dataSnapshot);
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        mFBDBListener.onFailure();
                    }
                });
    }

    //	public void addChangeListener(String userId)
//	{
//
//		Query allCoffeesQuery = fbDatabase.child("user-coffees").child(userId)
//				.orderByChild("rating");
//
//
//
//		allCoffeesQuery.addChildEventListener(new ChildEventListener() {
//			// TODO: implement the ChildEventListener methods as documented
//
//			@Override
//			public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
//				Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());
//
//				// A new comment has been added, add it to the displayed list
//				 Coffee c = dataSnapshot.getValue(Coffee.class);
//				 FBDManager.coffeeList.add(c);
//				//HashMap<String, Object> result = (HashMap<String, Object>)dataSnapshot.getValue();
//				Log.d(TAG, "dataSnapShop is :" + dataSnapshot);
//				Log.d(TAG, "coffeeList is now :" + FBDManager.coffeeList);
//
//				// ...
//			}
//
//			@Override
//			public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
//				Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
//
//				// A comment has changed, use the key to determine if we are displaying this
//				// comment and if so displayed the changed comment.
//				Coffee c = dataSnapshot.getValue(Coffee.class);
//				String coffeeKey = dataSnapshot.getKey();
//
//				// ...
//			}
//
//			@Override
//			public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
//				Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
//
//				// A comment has changed position, use the key to determine if we are
//				// displaying this comment and if so move it.
//				Coffee c = dataSnapshot.getValue(Coffee.class);
//				String coffeeKey = dataSnapshot.getKey();
//
//				// ...
//			}
//
//			@Override
//			public void onChildRemoved(DataSnapshot dataSnapshot) {
//				Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());
//
//				// A comment has changed, use the key to determine if we are displaying this
//				// comment and if so remove it.
//				String commentKey = dataSnapshot.getKey();
//
//				// ...
//			}
//
//			@Override
//			public void onCancelled(DatabaseError databaseError) {
//				Log.w(TAG, "loadCoffees:onCancelled", databaseError.toException());
//				//Toast.makeText(mContext, "Failed to load coffees.",
//				//		Toast.LENGTH_SHORT).show();
//			}
//		});
//	}
}
