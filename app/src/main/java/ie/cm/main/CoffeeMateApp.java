package ie.cm.main;

import android.app.Application;
import android.graphics.Bitmap;
import android.location.Location;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import ie.cm.api.FBDBManager;
import ie.cm.models.Coffee;

public class CoffeeMateApp extends Application
{
    private RequestQueue mRequestQueue;
    private static CoffeeMateApp mInstance;
    public List <Coffee>  coffeeList = new ArrayList<Coffee>();

    /* Client used to interact with Google APIs. */
    public GoogleApiClient      mGoogleApiClient;
    public GoogleSignInOptions  mGoogleSignInOptions;
    public Location             mCurrentLocation;
    public FirebaseAuth         mFirebaseAuth;
    public FirebaseUser         mFirebaseUser;

    public FBDBManager mFBDBManager;

    public boolean signedIn = false;
    public String googleToken;
    public String googleName;
    public String googleMail;
    public String googlePhotoURL;
    public Bitmap googlePhoto;
    public int drawerID = 0;

    public static final String TAG = CoffeeMateApp.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v("coffeemate", "CoffeeMate App Started");
        mInstance = this;
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        Log.v("coffeemate", "Adding Firebase offline persistence...");
        mFBDBManager = new FBDBManager();
        mFBDBManager.open();
    }

    public static synchronized CoffeeMateApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public <T> void add(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancel() {
        mRequestQueue.cancelAll(TAG);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


   // @Override
    public void onPause() {
        mInstance.onPause();
    }
}