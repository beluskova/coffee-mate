package ie.cm.adapters;

import android.util.Log;
import android.widget.Filter;
import com.google.firebase.database.Query;

import ie.cm.adapters.CoffeeListAdapter;
import ie.cm.fragments.CoffeeFragment;

public class CoffeeFilter extends Filter {
    private String 				filterText;
    private CoffeeListAdapter adapter;
    private Query				query;
    private CoffeeFragment		fragment;

    public CoffeeFilter(Query originalCoffeeQuery, String filterText
            ,CoffeeListAdapter adapter,CoffeeFragment fragment) {
        super();
        this.query = originalCoffeeQuery;
        this.filterText = filterText;
        this.adapter = adapter;
        this.fragment = fragment;
    }

    public void setFilter(String filterText) {
        this.filterText = filterText;
    }

    @Override
    protected FilterResults performFiltering(CharSequence prefix) {
        FilterResults results = new FilterResults();

        if(prefix != null)
            if (prefix.length() > 0)
                query = fragment.app.mFBDBManager.nameFilter(prefix.toString());

            else {
                if (filterText.equals("all")) {
                    query = fragment.app.mFBDBManager.getAllCoffees();
                } else if (filterText.equals("favourites")) {
                    query = fragment.app.mFBDBManager.getFavouriteCoffees();
                }
            }
        results.values = query;

        return results;
    }

    @Override
    protected void publishResults(CharSequence prefix, FilterResults results) {

        fragment.updateUI((Query) results.values);
    }
}

