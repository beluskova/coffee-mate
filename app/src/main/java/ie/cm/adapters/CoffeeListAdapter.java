package ie.cm.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.Query;

import java.text.DecimalFormat;
import java.util.List;

import ie.cm.R;
import ie.cm.models.Coffee;

//https://github.com/firebase/FirebaseUI-Android/blob/master/README.md

//https://github.com/firebase/FirebaseUI-Android/blob/master/database/README.md

//https://groups.google.com/forum/#!topic/firebase-talk/O1kJ4VLUg10

public class CoffeeListAdapter extends FirebaseListAdapter<Coffee> {

  private OnClickListener deleteListener;
  public Query query ;

  public CoffeeListAdapter(Activity context, OnClickListener deleteListener,
                           Query query) {
    super(context, Coffee.class,R.layout.coffeerow, query);
    Log.v("coffeemate","Creating Adapter with :" + query);
    this.deleteListener = deleteListener;
    this.query = query;
  }

  @Override
  protected void populateView(View row, Coffee coffee,int position) {
    Log.v("coffeemate","Populating View Adapter with :" + coffee);
    //Set the rows TAG to the coffee 'key'
    row.setTag(getRef(position).getKey());

    ((TextView) row.findViewById(R.id.rowCoffeeName)).setText(coffee.name);
    ((TextView) row.findViewById(R.id.rowCoffeeShop)).setText(coffee.shop);
    ((TextView) row.findViewById(R.id.rowRating)).setText(coffee.rating + " *");
    ((TextView) row.findViewById(R.id.rowPrice)).setText("€" +
            new DecimalFormat("0.00").format(coffee.price));

    ImageView imgIcon = (ImageView) row.findViewById(R.id.RowImage);

    if (coffee.favourite == true)
      imgIcon.setImageResource(R.drawable.ic_favourite_on);
    else
      imgIcon.setImageResource(R.drawable.ic_favourite_off);


    ImageView imgDelete = (ImageView) row.findViewById(R.id.imgDelete);
    imgDelete.setTag(getRef(position).getKey());
    imgDelete.setOnClickListener(deleteListener);
  }
}
