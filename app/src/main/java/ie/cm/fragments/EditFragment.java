package ie.cm.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;

import java.util.List;

import ie.cm.R;
import ie.cm.activities.Home;
import ie.cm.api.CoffeeApi;
import ie.cm.api.FBDBListener;
import ie.cm.api.VolleyListener;
import ie.cm.main.CoffeeMateApp;
import ie.cm.models.Coffee;

import static ie.cm.main.CoffeeMateApp.TAG;


public class EditFragment extends Fragment implements FBDBListener {

    private OnFragmentInteractionListener mListener;
    TextView    titleBar,titleName,titleShop;
    Coffee      aCoffee;
    Boolean     isFavourite;

    EditText    name, shop, price;
    RatingBar   ratingBar;
    ImageView   favouriteImage;

    String      coffeeKey;
    String      s;

    public CoffeeMateApp app = CoffeeMateApp.getInstance();

    public EditFragment() {
        // Required empty public constructor
    }

    public static EditFragment newInstance(Bundle coffeeBundle) {
        EditFragment fragment = new EditFragment();
        fragment.setArguments(coffeeBundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit, container, false);

        titleName = ((TextView)v.findViewById(R.id.coffeeNameTextView));
        titleShop = ((TextView)v.findViewById(R.id.coffeeShopTextView));
        name = (EditText)v.findViewById(R.id.nameEditText);
        shop = (EditText)v.findViewById(R.id.shopEditText);
        price = (EditText)v.findViewById(R.id.priceEditText);
        ratingBar = (RatingBar) v.findViewById(R.id.coffeeRatingBar);
        favouriteImage = (ImageView) v.findViewById(R.id.favouriteImageView);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        app.mFBDBManager.attachListener(this);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        app.mFBDBManager.attachListener(this);

        if(getArguments() != null) {
            coffeeKey = getArguments().getString("coffeeKey");
            Log.v("coffeemate","getACoffee() snapshot is " + aCoffee + " for user : " + app.mFBDBManager + "coffeKey: " + coffeeKey);
            app.mFBDBManager.getACoffee(coffeeKey);

        }
        titleBar = (TextView) getActivity().findViewById(R.id.recentAddedBarTextView);
        titleBar.setText(R.string.updateACoffeeLbl);
    }

    @Override
    public void onSuccess(DataSnapshot dataSnapshot) {
        aCoffee = dataSnapshot.getValue(Coffee.class);
        Log.v("coffeemate","getACoffee() snapshot is " + aCoffee + " for user : " + app.mFBDBManager.mFBUserId);
        if(aCoffee != null)
            updateUI();
    }

    @Override
    public void onFailure() {
        Log.v("coffeemate", "Unable to Read/Update Coffee Data");
        Toast.makeText(getActivity(),"Unable to Read/Update Coffee Data",Toast.LENGTH_LONG).show();
    }

    public interface OnFragmentInteractionListener {
        void toggle(View v);
        void update(View v);
    }

    public void toggle(View v) {

        if (isFavourite) {
            aCoffee.favourite = false;
            toastMessage("Removed From Favourites");
            isFavourite = false;
            favouriteImage.setImageResource(R.drawable.ic_favourite_off);
        } else {
            aCoffee.favourite = true;
            toastMessage("Added to Favourites !!");
            isFavourite = true;
            favouriteImage.setImageResource(R.drawable.ic_favourite_on);
        }

        app.mFBDBManager.toggleFavourite(coffeeKey,isFavourite);
    }

    public void update(View v) {

            toastMessage("mListener not null!!");
            String coffeeName = name.getText().toString();
            String coffeeShop = shop.getText().toString();
            String coffeePriceStr = price.getText().toString();
            double ratingValue = ratingBar.getRating();

            double coffeePrice;
            try {
                coffeePrice = Double.parseDouble(coffeePriceStr);
            } catch (NumberFormatException e)
            {            coffeePrice = 0.0;        }

                aCoffee.name = coffeeName;
                aCoffee.shop = coffeeShop;
                aCoffee.price = coffeePrice;
                aCoffee.rating = ratingValue;

                if ((aCoffee.name.length() > 0) && (aCoffee.shop.length() > 0)) {

                    app.mFBDBManager.updateACoffee(coffeeKey,aCoffee);
                    //return to the home screen
                    Intent intent = new Intent(getActivity(), Home.class);
                    startActivity(intent);

        } else
            toastMessage("You must Enter Something for Name and Shop");
    }

    protected void toastMessage(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
    }

    public void updateUI() {
        titleName.setText(aCoffee.name);
        titleShop.setText(aCoffee.shop);
        name.setText(aCoffee.name);
        shop.setText(aCoffee.shop);
        price.setText(""+aCoffee.price);
        ratingBar.setRating((float)aCoffee.rating);

        if (aCoffee.favourite == true) {
            favouriteImage.setImageResource(R.drawable.ic_favourite_on);
            isFavourite = true;
        } else {
            favouriteImage.setImageResource(R.drawable.ic_favourite_off);
            isFavourite = false;
        }
    }

}

