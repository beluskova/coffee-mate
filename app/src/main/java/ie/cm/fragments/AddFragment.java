package ie.cm.fragments;


import android.app.Fragment;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import ie.cm.R;
import ie.cm.activities.Home;
import ie.cm.api.CoffeeApi;
import ie.cm.main.CoffeeMateApp;
import ie.cm.models.Coffee;

import static ie.cm.api.CoffeeApi.TAG;

public class AddFragment extends Fragment implements View.OnClickListener ,OnMapReadyCallback {

    private TextView    titleBar;
    private String 		coffeeName, coffeeShop;
    private double 		coffeePrice, ratingValue;
    private EditText    name, shop, price;
    private RatingBar   ratingBar;
    public CoffeeMateApp app = CoffeeMateApp.getInstance();

    public AddFragment() {
        // Required empty public constructor
    }

    public static AddFragment newInstance() {
        AddFragment fragment = new AddFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_add, container, false);

        Button saveButton = (Button) v.findViewById(R.id.saveCoffeeBtn);
        name = (EditText) v.findViewById(R.id.nameEditText);
        shop = (EditText) v.findViewById(R.id.shopEditText);
        price = (EditText) v.findViewById(R.id.priceEditText);
        ratingBar = (RatingBar) v.findViewById(R.id.coffeeRatingBar);
        saveButton.setOnClickListener(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        titleBar = (TextView) getActivity().findViewById(R.id.recentAddedBarTextView);
        titleBar.setText(R.string.addACoffeeLbl);
        Log.v("add fragment:", "Adding a new coffee from user" + app.mFBDBManager.mFBUserId);
    }

    public void onClick(View v) {

        coffeeName = name.getText().toString();
        coffeeShop = shop.getText().toString();
        try {
            coffeePrice = Double.parseDouble(price.getText().toString());
        } catch (NumberFormatException e) {
            coffeePrice = 0.0;
        }
        ratingValue = ratingBar.getRating();

        if ((coffeeName.length() > 0) && (coffeeShop.length() > 0)
                && (price.length() > 0)) {
            Coffee c = new Coffee(app.mFBDBManager.mFBUserId, coffeeName, coffeeShop, ratingValue,
                    coffeePrice,
                    false,
                    app.googleToken,
                    app.mCurrentLocation.getLatitude(),
                    app.mCurrentLocation.getLongitude(),
                    app.googlePhotoURL,  getAddressFromLocation( app.mCurrentLocation));
            Log.v("add fragment:", "Adding a new coffee" + c);
            Log.v("add fragment:", "Adding a new coffee from user" + app.mFBDBManager.mFBUserId);
            app.mFBDBManager.addCoffee(c);
            resetFields();
            Toast.makeText(
                    getActivity(),
                    "A new coffee " + c.name + " added, returning home",
                    Toast.LENGTH_SHORT).show();
            //returning to home page
            Intent intent = new Intent(getActivity(), Home.class);
            startActivity(intent);

        } else
            Toast.makeText(
                    getActivity(),
                    "You must Enter Something for "
                            + "\'Name\', \'Shop\' and \'Price\'",
                    Toast.LENGTH_SHORT).show();
    }

    private void resetFields() {
        name.setText("");
        shop.setText("");
        price.setText("");
        ratingBar.setRating(2);
        name.requestFocus();
        name.setFocusable(true);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        addCoffees(app.coffeeList,googleMap);
    }

    public void addCoffees(List<Coffee> list, GoogleMap mMap)
    {
        for(Coffee c : list)
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(c.marker.coords.latitude, c.marker.coords.longitude))
                    .title(c.name + " €" + c.price)
                    .snippet(c.shop + " " + c.address)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.coffee_icon)));

    }

    private String getAddressFromLocation( Location location ) {
        Geocoder geocoder = new Geocoder( getActivity() );

        String strAddress = "";
        Address address;
        try {
            address = geocoder
                    .getFromLocation( location.getLatitude(), location.getLongitude(), 1 )
                    .get( 0 );
            strAddress = address.getAddressLine(0) +
                    " " + address.getAddressLine(1) +
                    " " + address.getAddressLine(2);
        }
        catch (IOException e ) {
        }

        return strAddress;
    }
}
